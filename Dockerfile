FROM alpine
COPY entrypoint.sh /opt/entrypoint.sh
RUN chmod +x /opt/entrypoint.sh
EXPOSE 1080
ENTRYPOINT ["sh", "-c", "/opt/entrypoint.sh"]
