#!/bin/sh

## Install brook
wget -q https://github.com/txthinking/brook/releases/latest/download/brook_linux_amd64 -O /usr/local/bin/brook
chmod +x /usr/local/bin/brook

## Start service
./usr/local/bin/brook wsserver --listen :1080 --path ${path} --password ${passwd}
